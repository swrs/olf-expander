# OLF Expander
OLF Expander is a fontforge script to expand a font's outline with a variable stroke width. The OLF Expander was intended to work with "one-line" fonts in order to generate multiple weights…

```
  /----\  |     |---      ///====\\\  |||    |||===      /////||||\\\\\  |||||    ||||||||
  |    |  |     |         |||    |||  |||    |||         |||||    |||||  |||||    |||||
  |    |  |     |---  =>  |||    |||  |||    |||===  =>  |||||    |||||  |||||    ||||||||
  |    |  |     |         |||    |||  |||    |||         |||||    |||||  |||||    |||||
  \----/  |---  |         \\\====///  |||=== |||         \\\\\||||/////  |||||||| |||||
```

… but it can expand regular fonts or generate outline versions just as well…

```
/ /----\ \  | |     | |---      // //====\\ \\  || ||     || ||===
| |    | |  | |     | |---      || ||    || ||  || ||     || ||===
| |    | |  | |     | |___  =>  || ||    || ||  || ||     || ||===
| |    | |  | |___  | |---      || ||    || ||  || ||===  || ||===
\ \----/ /  | |---  | |         \\ \\====// //  || ||===  || ||
```

… here an example specimen of the *OLF SimpleSans* expanded to four different weights…

![](images/olf-simplesans_weights.png)

… further example conversions of an *Arial Narrow*, *Computer Modern*, etc. can be found in the [images/](images/) directory.

## Installation
OLF Expander uses Fontforge to expand the stroke and convert the output to an outline font format, therefore the `fontforge` command line utilities need to be installed.

## Usage
Clone the repository and move to its directory in your favourite command line. OLF Expander is executed with a bash script and takes these mandatory arguments `skeleton`, `fontname`, `weight`, `format`, `output-dir`:

```bash
$ cd olf-expander/
$ bin/expander skeleton fontname weight format [output-dir removeinternal removeexternal]
```

- `skeleton` (string): Path to the one-line source font (absolute or relative path)
- `fontname` (string): Sets the font- and output-filename
- `weight` (integer): Sets the stroke width (min. 10)
- `format` (string): Sets the output format (any format Fontforge can generate: .ttf, .woff, .woff2, .otf, etc.)
- `output-dir` (string): Directory to save the output to (dir will be created if non-existant)
- `removeinternal` (boolean): Remove internal contour after expanding stroke (default: false). This could be useful to render a bolder version of a font which is not a one-line font.
- `removeexternal` (boolean): Remove external contour after expanding stroke (default: false).

```bash
$ bin/expander one-line-font.sfd MyFont 50 .ttf
# => creates output from relative path one-line-font.sfd with width 50
# => and outputs a TTF to ./MyFont-50.ttf

$ bin/expander /user/fonts/another-line-font.ttf MyFont 140 .woff ./output
# => creates output from absolute path /user/fonts/another-line-font.ttf with a width of 140
# => and outputs a WOFF to ./output MyFont-140.woff

$ bin/expander another-line-font.ttf MyFont 60 .ttf ./output True
# => creates output from relatove path another-line-font.ttf with a width of 60,
# => outputs a TTF to ./output MyFont-60.woff, and removes internal stroke
```

## Version
1.1.2
