#!/bin/bash

# $1, $2, $3 args are mandatory
# $1 arg sets path to source font file
# $2 arg sets fontname
# $3 arg sets weight as integer

if [[ $# -lt 3 ]]; then
  echo "Mandatory arguments are missing.
  Usage: bin/expander source fontname weight [format output_dir]
  Example: bin/expander ./myfont.ttf MyFont-Expanded 25 .ttf ./output"
  exit 1
fi

# $1 argument sets source font file
SKELETON="$(cd $(dirname $1); pwd)/$(basename $1)"

if ! [[ -f $SKELETON ]]; then
  echo "$SKELETON does not exist"
  exit 1
fi

FONTNAME=$2
WEIGHT=$3

# $4 argument sets output format (deafult: .ttf)
if [[ $# -lt 4 ]]; then FORMAT=".ttf"; else FORMAT=$4; fi

# $5 argument sets output directory
# creates it if non-existent
if [[ $# -lt 5 ]]; then
  OUTPUT_DIR=$(pwd)
else
  if ! [[ -d $5 ]]; then mkdir $5; fi
  OUTPUT_DIR=$(cd $5; pwd)
fi

# $6 argument sets "removeinternal" flag for stroke expansion
if [[ $# -lt 6 ]]; then REMOVEINTERNAL="false"; else REMOVEINTERNAL="true"; fi

# $7 argument sets "removeinternal" flag for stroke expansion
if [[ $# -lt 7 ]]; then REMOVEEXTERNAL="false"; else REMOVEEXTERNAL="true"; fi


echo $SKELETON
echo "Source file: $SKELETON" \
  && echo "Fontname: $FONTNAME" \
  && echo "Weight: $WEIGHT" \
  && echo "Format: $FORMAT" \
  && echo "Output dir: $OUTPUT_DIR"

# call fontforge python script with args
SCRIPT="$(cd $(dirname $BASH_SOURCE); pwd)/../src/script.py"
# python3 $SCRIPT $SKELETON $FONTNAME $WEIGHT $FORMAT $OUTPUT_DIR $REMOVEINTERNAL $REMOVEEXTERNAL
fontforge -lang=py -script $SCRIPT $SKELETON $FONTNAME $WEIGHT $FORMAT $OUTPUT_DIR $REMOVEINTERNAL $REMOVEEXTERNAL
