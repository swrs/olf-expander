# Changelog

## 1.1.2
- Fix `removeinternal` and `removeexternal` flags

## 1.1.1
- Add `removeinternal` and `removeexternal` flags for stroke expansion

## 1.1.0
- Rewrote Fontforge script as Python script `src/script.py`.

## 1.0.0
- OLF Expander bash and Fontforge scripts.
