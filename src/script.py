#!/usr/bin/env python3

import sys
import fontforge

file = sys.argv[1]
name = sys.argv[2]
weight = sys.argv[3]
output_format = sys.argv[4]
output_dir = sys.argv[5]
removeinternal = True if sys.argv[6].capitalize() == "True" else False
removeexternal = True if sys.argv[7].capitalize() == "True" else False
font_fullname = f"{name} {weight}"
font_fontname = f"{name.replace(' ', '-')}--{weight}"

log = f"""
Parameters:\n
file: {file}
name: {name}
font family: {name}
font fullname: {font_fullname}
font name: {font_fontname}
weight: {weight}
output_format: {output_format}
output_dir: {output_dir}
removeinternal: {removeinternal}
removeexternal: {removeexternal}
"""

print(log)

# open font
font = fontforge.open(sys.argv[1])

# set name and weight
font.fullname = font_fullname
font.fontname = font_fontname
font.familyname = font_fontname
font.weight = weight

# select glyhps
font.selection.all()

# expand stroke
font.stroke("circular", int(weight), removeinternal=removeinternal, removeexternal=removeexternal)
font.correctDirection()
font.removeOverlap()

# save font
output_path = f"{output_dir}/{name.replace(' ', '-')}-{weight}{output_format}"
font.generate(output_path)
print(f"Generated font at {output_path}")
